import { PontoTuristico } from "./components/PontoTuristico";
import { PreviewImage } from "./components/PreviewImagem";

document.addEventListener("DOMContentLoaded", function () {
    new PontoTuristico();
    new PreviewImage();
});
