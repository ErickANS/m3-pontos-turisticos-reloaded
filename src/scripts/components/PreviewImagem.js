export class PreviewImage {
    constructor() {
        this.selectors();
        this.events();
    }

    selectors() {
        this.imageInput = document.querySelector(".tourist-spot-image-input");
        this.preview = document.querySelector(".tourist-spot-image-preview");
        this.imageSpan = document.querySelector(".tourist-spot-image-span");
    }

    events() {
        this.imageInput.addEventListener("change", this.showPreview.bind(this));
    }

    showPreview() {
        const imagePreview = this.preview;
        const reader = new FileReader();
        reader.onload = function () {
            console.log(imagePreview);
            sessionStorage.setItem("imageURL", reader.result);
            imagePreview.src = reader.result;
        };
        reader.readAsDataURL(this.imageInput.files[0]);

        this.imageSpan.style.display = "none";
    }
}
