export class PontoTuristico {
    constructor() {
        this.list = [];

        this.selectors();
        this.events();
        this.getSpotFromLS();
    }
    selectors() {
        this.form = document.querySelector(".tourist-spot-form");
        this.imageInput = document.querySelector(".tourist-spot-image-input");
        this.titleInput = document.querySelector(".tourist-spot-title-input");
        this.preview = document.querySelector(".tourist-spot-image-preview");
        this.descriptionInput = document.querySelector(".tourist-spot-description-input");
        this.newSpots = document.querySelector(".new-tourist-spots-wrapper");
        this.imageSpan = document.querySelector(".tourist-spot-image-span");
    }

    events() {
        this.form.addEventListener("submit", this.addSpotToList.bind(this));
    }

    getSpotFromLS() {
        if (localStorage.list !== undefined) {
            this.list = JSON.parse(localStorage.list);
            this.renderListSpots();
        }
    }

    addSpotToList(event) {
        event.preventDefault();

        const spotImage = sessionStorage.getItem("imageURL");
        const spotTitle = event.target["spot-title"].value;
        const spotDescription = event.target["spot-description"].value;

        if (spotImage !== "" && spotTitle !== "" && spotDescription !== "") {
            const spot = {
                image: spotImage,
                title: spotTitle,
                description: spotDescription,
            };

            this.list.push(spot);
            localStorage.setItem("list", JSON.stringify(this.list));
            this.renderListSpots();
            this.resetInputs();
        }
    }

    renderListSpots() {
        let spotsStructure = "";

        this.list.forEach(function (spot) {
            spotsStructure += `
                        <div class="tourist-spot">
                            <img class="tourist-spot-image" src="${spot.image}" alt="">
                            <div class="tourist-spot-text">
                                <h2 class="tourist-spot-title">${spot.title}</h2>
                                <p class="tourist-spot-description">
                                ${spot.description}
                                </p>
                            </div>
                        </div>
                        `;
        });
        this.newSpots.innerHTML = spotsStructure;
    }

    resetInputs() {
        this.imageSpan.style.display = "inline-block";
        this.preview.src = "";
        this.imageInput.value = "";
        this.titleInput.value = "";
        this.descriptionInput.value = "";
    }
}
